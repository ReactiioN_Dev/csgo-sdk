﻿#include "sdk.h"
#include "vthook.h"
#include "netvars.h"
#include "sigs.h"
#include "utils.h"
#include "xorstr.h"

IBaseClientDLL* clientdll;
IClientEntityList* entitylist;
IVEngineClient* engine;
IEngineTrace* enginetrace;
IVModelInfoClient* modelinfo;
IVModelRender* modelrender;
IVRenderView* render;
IViewRender* view;
IFileSystem* filesystem;
IVDebugOverlay* g_pDebugOverlay;
IVDebugOverlay* debugoverlay;

CInput* g_pInput;
CViewRender* g_pViewRender;
CGlobalVars* g_pGlobals;

ClientModeShared* g_pClientMode;
IMatSystemSurface* g_pMatSurface;
CGlowObjectManager* g_pGlowObjectManager;

DWORD g_dwGameResources;
DWORD* g_pWeaponDatabase;

CVMTHookManager* pEngineHook;
CVMTHookManager* pPanelHook;
CVMTHookManager* pStudioRenderHook;
CVMTHookManager* pMDLCacheHook;
CVMTHookManager* pSurfaceHook;
CVMTHookManager* pClientHook;
CVMTHookManager* pClientModeHook;
CVMTHookManager* pModelRenderHook;
CVMTHookManager* pInputHook;

MDLHandle_t hAK47;

tPaintTraverse oPaintTraverse;
tBeginFrame oBeginFrame;
tFindMDL oFindMDL;
tDrawModel oDrawModel;
tWriteUsercmdDeltaToBuffer oWriteUsercmdDeltaToBuffer;
tCreateMove oCreateMove;

CreateInterfaceFn CvarFactory = NULL;
CreateInterfaceFn DatacacheFactory = NULL;
CreateInterfaceFn EngineFactory = NULL;
CreateInterfaceFn ClientFactory = NULL;
CreateInterfaceFn VGUIFactory = NULL;
CreateInterfaceFn VGUI2Factory = NULL;
CreateInterfaceFn MatSysFactory = NULL;
CreateInterfaceFn StudioRenderFactory = NULL;
CreateInterfaceFn InputSystemFactory = NULL;
CreateInterfaceFn FileSysFactory = NULL;

bool name;
int screenWidth, screenHeight;

DWORD dwDebugOverlayPanelOffset = 0x00; // client{A3 ?? ?? ?? ?? 5E C3 5F}+1
DWORD dwEngineRendererOffset = 0x00; // engine{B9 ?? ?? ?? ?? E8 ?? ?? ?? ?? 8D 4D F4}+1
DWORD dwDemoRecorder = 0x00; // engine{8B 0D ? ? ? ? 8B 01 FF 50 3C 8B 0D ? ? ? ? 8B 01}+2
DWORD dwHLTVServer = 0x00; // engine{8B 0D ?? ?? ?? ?? 85 C9 74 24 8D 45 A0}+2 or engine{8B 0D ? ? ? ? 83 C4 0C 85 C9 74 4B}+2

void SpitShit();
void GetNetVars();

void DrawString(int x, int y, Color clr, vgui::HFont font, const char *pszText)
{
	if (pszText == NULL)
		return;
	wchar_t szString[1024] = { '\0' };
	wsprintfW(szString, L"%S", pszText);
	g_pMatSurface->DrawSetTextPos(x, y);
	g_pMatSurface->DrawSetTextFont(font);
	g_pMatSurface->DrawSetTextColor(clr);
	g_pMatSurface->DrawPrintText(szString, wcslen(szString));
}

bool WorldToScreen(Vector &vOrigin, Vector &vScreen)
{
	const VMatrix& worldToScreen = engine->WorldToScreenMatrix();

	float w = worldToScreen[3][0] * vOrigin[0] + worldToScreen[3][1] * vOrigin[1] + worldToScreen[3][2] * vOrigin[2] + worldToScreen[3][3];
	vScreen.z = 0;
	if (w > 0.01)
	{
		float inverseWidth = 1 / w;
		vScreen.x = (screenWidth / 2) + (0.5 * ((worldToScreen[0][0] * vOrigin[0] + worldToScreen[0][1] * vOrigin[1] + worldToScreen[0][2] * vOrigin[2] + worldToScreen[0][3]) * inverseWidth) * screenWidth + 0.5);
		vScreen.y = (screenHeight / 2) - (0.5 * ((worldToScreen[1][0] * vOrigin[0] + worldToScreen[1][1] * vOrigin[1] + worldToScreen[1][2] * vOrigin[2] + worldToScreen[1][3]) * inverseWidth) * screenHeight + 0.5);
		return true;
	}
	return false;
}

bool shouldSendPacket = true;
float flInputSampleTime = 0;
DWORD dwCmd = 0;
DWORD dwRet = 0;
bool __declspec(naked) __fastcall hkCreateMove(/*void* ecx, void* edx, float flInputSampleTime, CUserCmd* cmd*/)
{
	__asm
	{
		push edx // preserve edx so we can use it to get retval and cmd
		mov edx, [esp + 4]
		mov dwRet, edx
		mov edx, [esp + 0xC] // dwCmd is at (ESP) stackptr + 0xC (ESP + 0x10 is CMDLCache vtable for some reason)
							 // (ESP) stackptr + 0x8 is flInputSampleTime but we don't need it
		mov dwCmd, edx
		pop edx // restore edx value from stack
		add esp, 4 // clean stack
		call oCreateMove // call the original func

		//mov shouldSendPacket, bl

		pushad // push all registers onto the stack
	}

	//printf("yolooo: %i\n", GetWeaponInfoHandle((void*)g_pWeaponDatabase, "weapon_knife"));
	__asm
	{
		popad // pop all 
		//mov bl, shouldSendPacket
		jmp dwRet
	}
}

void __fastcall hkPaintTraverse(void* ecx, void* edx, unsigned int vguiPanel, bool forceRepaint, bool allowForce)
{
	oPaintTraverse(ecx, vguiPanel, forceRepaint, allowForce);

	static unsigned int MatSystemTopPanel;
	static vgui::HFont s_hFontPlayer;

	if (!MatSystemTopPanel)
	{
		const char* szName = ipanel()->GetName(vguiPanel);
		//g_pCVar->ConsoleColorPrintf(Color::Blue(), "panel: %s\n", szName);
		if (_V_stricmp(szName, "MatSystemTopPanel") == 0) // MatSystemTopPanel
		{
			MatSystemTopPanel = vguiPanel;

			engine->GetScreenSize(screenWidth, screenHeight);
			s_hFontPlayer = g_pMatSurface->CreateFont();
			g_pMatSurface->SetFontGlyphSet(s_hFontPlayer, "Tahoma", 14, 150, 0, 0, FONTFLAG_OUTLINE);
		}
	} 

	if (MatSystemTopPanel == vguiPanel)
	{

		//__asm
		//printf("yolooo: %i\n", GetWeaponInfoHandle("weapon_knife"));

		if (GetAsyncKeyState(VK_F9) & 1)
		{
			name = !name;
			if (name) Beep(0x367, 200);
			else Beep(0x255, 200);
		}

		if (engine->IsInGame() && engine->IsConnected() && !engine->IsTakingScreenshot())
		{
			C_BaseEntity *pLocalEntity = (C_BaseEntity*)entitylist->GetClientEntity(engine->GetLocalPlayer());
			if (!pLocalEntity)
				return;

			for (int i = 0; i < entitylist->GetHighestEntityIndex(); i++)
			{
				C_BaseEntity* pBaseEntity = (C_BaseEntity*)entitylist->GetClientEntity(i);
				if (!pBaseEntity)
					continue;
				if (pBaseEntity->health() < 1)
					continue;
				if (pBaseEntity == pLocalEntity)
					continue;
				if (pLocalEntity->team() == pBaseEntity->team())
					continue;
				if (pBaseEntity->m_bDormant)
					continue;

				//if (name)
				//{
				//	Vector out;
				//	if (WorldToScreen(pBaseEntity->GetAbsOrigin(), out))
				//	{
				//		player_info_t info;
				//		engine->GetPlayerInfo(i, &info);
				//		DrawString(out.x - 5, out.y, Color::Red(), s_HFontPlayer, info.name);
				//	}
				//}
			}
		}
	}
}

// Can switch models using this
MDLHandle_t __fastcall hkFindMDL(void* ecx, void* edx, const char* pMDLRelativePath)
{
	return oFindMDL(ecx, pMDLRelativePath);
}

// r_drawothermodels style chams
void __fastcall hkDrawModel(void* ecx, void* edx, DrawModelResults_t *pResults, const DrawModelInfo_t& info, matrix3x4_t *pBoneToWorld, float *pFlexWeights, float *pFlexDelayedWeights, const Vector &modelOrigin, int flags)
{
	C_BaseEntity* pBaseEntity = (C_BaseEntity*)info.m_pClientEntity;
	if (pBaseEntity)
	{
		if (pBaseEntity->health() < 1 )
		{
			oDrawModel(ecx, pResults, info, pBoneToWorld, pFlexWeights, pFlexDelayedWeights, modelOrigin, flags);
			return;
		}
		C_BaseEntity* pLocalEntity = (C_BaseEntity*)entitylist->GetClientEntity(engine->GetLocalPlayer());
		if (pLocalEntity)
		{
			if (pBaseEntity->team() != pLocalEntity->team())
				flags |= STUDIORENDER_DRAW_WIREFRAME;
		}
	}
	oDrawModel(ecx, pResults, info, pBoneToWorld, pFlexWeights, pFlexDelayedWeights, modelOrigin, flags);
}

//--------------------------------------Hook-Thread-------------------------------------------
DWORD WINAPI Init(LPVOID lpArguments)
{
	offys.dwClientBase = (DWORD)GetModuleHandle("client.dll");
	offys.dwEngineBase = (DWORD)GetModuleHandle("engine.dll");
	offys.dwFileSysBase = (DWORD)GetModuleHandle("filesystem_stdio.dll");
	offys.dwVGuiBase = (DWORD)GetModuleHandle("vguimatsurface.dll");
	offys.dwVGui2Base = (DWORD)GetModuleHandle("vgui2.dll");
	offys.dwMatSysBase = (DWORD)GetModuleHandle("materialsystem.dll");
	offys.dwStudioBase = (DWORD)GetModuleHandle("studiorender.dll");
	offys.dwInputSysBase = (DWORD)GetModuleHandle("inputsystem.dll");
	offys.dwVStdBase = (DWORD)GetModuleHandle("vstdlib.dll");
	offys.dwDataCacheBase = (DWORD)GetModuleHandle("datacache.dll");

	EngineFactory		= (CreateInterfaceFn)GetProcAddress((HMODULE)offys.dwEngineBase, CREATEINTERFACE_PROCNAME);
	ClientFactory		= (CreateInterfaceFn)GetProcAddress((HMODULE)offys.dwClientBase, CREATEINTERFACE_PROCNAME);
	FileSysFactory		= (CreateInterfaceFn)GetProcAddress((HMODULE)offys.dwFileSysBase, CREATEINTERFACE_PROCNAME);
	VGUIFactory			= (CreateInterfaceFn)GetProcAddress((HMODULE)offys.dwVGuiBase, CREATEINTERFACE_PROCNAME);
	VGUI2Factory		= (CreateInterfaceFn)GetProcAddress((HMODULE)offys.dwVGui2Base, CREATEINTERFACE_PROCNAME);
	MatSysFactory		= (CreateInterfaceFn)GetProcAddress((HMODULE)offys.dwMatSysBase, CREATEINTERFACE_PROCNAME);
	StudioRenderFactory = (CreateInterfaceFn)GetProcAddress((HMODULE)offys.dwStudioBase, CREATEINTERFACE_PROCNAME);
	InputSystemFactory	= (CreateInterfaceFn)GetProcAddress((HMODULE)offys.dwInputSysBase, CREATEINTERFACE_PROCNAME);
	CvarFactory			= (CreateInterfaceFn)GetProcAddress((HMODULE)offys.dwVStdBase, CREATEINTERFACE_PROCNAME);
	DatacacheFactory	= (CreateInterfaceFn)GetProcAddress((HMODULE)offys.dwDataCacheBase, CREATEINTERFACE_PROCNAME);


	engine				= (IVEngineClient*)EngineFactory(VENGINE_CLIENT_INTERFACE_VERSION, NULL);
	clientdll			= (IBaseClientDLL*)ClientFactory(CLIENT_DLL_INTERFACE_VERSION, NULL);
	enginetrace			= (IEngineTrace*)EngineFactory(INTERFACEVERSION_ENGINETRACE_CLIENT, NULL);
	modelinfo			= (IVModelInfoClient*)EngineFactory(VMODELINFO_CLIENT_INTERFACE_VERSION, NULL);
	entitylist			= (IClientEntityList*)ClientFactory(VCLIENTENTITYLIST_INTERFACE_VERSION, NULL);
	g_pVGuiSurface		= (vgui::ISurface*)VGUIFactory(VGUI_SURFACE_INTERFACE_VERSION, NULL);
	g_pMatSurface		= (IMatSystemSurface*)g_pVGuiSurface->QueryInterface(MAT_SYSTEM_SURFACE_INTERFACE_VERSION);
	g_pVGuiPanel		= (vgui::IPanel*)VGUI2Factory(VGUI_PANEL_INTERFACE_VERSION, NULL);
	g_pVGuiInput		= (vgui::IInput*)VGUI2Factory(VGUI_INPUT_INTERFACE_VERSION, NULL);
	g_pVGui				= (vgui::IVGui*)VGUI2Factory(VGUI_IVGUI_INTERFACE_VERSION, NULL);
	g_pVGuiSystem		= (vgui::ISystem*)VGUI2Factory(VGUI_SYSTEM_INTERFACE_VERSION, NULL);
	g_pInputSystem		= (IInputSystem*)InputSystemFactory(INPUTSYSTEM_INTERFACE_VERSION, NULL);
	g_pCVar				= (ICvar*)CvarFactory(CVAR_INTERFACE_VERSION, NULL);
	filesystem			= (IFileSystem*)FileSysFactory(BASEFILESYSTEM_INTERFACE_VERSION, NULL);
	materials			= (IMaterialSystem*)MatSysFactory(MATERIAL_SYSTEM_INTERFACE_VERSION, NULL);
	studiorender		= (IStudioRender*)StudioRenderFactory(STUDIO_RENDER_INTERFACE_VERSION, NULL);
	modelrender			= (IVModelRender*)EngineFactory(VENGINE_HUDMODEL_INTERFACE_VERSION, NULL);
	render				= (IVRenderView*)EngineFactory(VENGINE_RENDERVIEW_INTERFACE_VERSION, NULL);
	mdlcache			= (IMDLCache*)DatacacheFactory(MDLCACHE_INTERFACE_VERSION, NULL);
	debugoverlay		= (IVDebugOverlay*)EngineFactory(VDEBUG_OVERLAY_INTERFACE_VERSION, NULL);
	
	g_pStudioRender = studiorender;
	g_pMDLCache = mdlcache;
	g_pDebugOverlay = debugoverlay;


	for (int i = 0; i < 42; i++)
	{
		FileWeaponInfo_t* info = GetFileWeaponInfoFromHandle(i);
		printf("(%i) %s: 0x%X\n", i, info->szClassName, info);
	}

	g_pGlobals = **(CGlobalVars***)(utils::FindPattern(offys.dwClientBase, 0x7CE000, sigGlobalVars, sizeof(sigGlobalVars)) + 1);

	do g_pClientMode = **(ClientModeShared***)(utils::FindPattern(offys.dwClientBase, 0x7CE000, sigClientMode, sizeof(sigClientMode)) + 0x2);
	while (!g_pClientMode);

	dwDebugOverlayPanelOffset = /**(DWORD**)*/(*(DWORD*)(utils::FindPattern(offys.dwClientBase, 0x7CE000, sigDebugOverlayPanel, sizeof(sigDebugOverlayPanel)) + 1));

	//vgui::HFont s_HFontPlayer = g_pMatSurface->CreateFont();
	//g_pMatSurface->SetFontGlyphSet(s_HFontPlayer, "Tahoma", 14, 150, 0, 0, FONTFLAG_OUTLINE);
	//void* pDebugOverlayPanel = *(DWORD**)(dwDebugOverlayPanelOffset);
	//*(vgui::HFont*)((DWORD)pDebugOverlayPanel + 0x154) = s_HFontPlayer;
	//g_pCVar->ConsoleColorPrintf(Color::Green(), "dwDebugOverlayPanelOffset: 0x%X\n", dwDebugOverlayPanelOffset);
	//g_pCVar->ConsoleColorPrintf(Color::Green(), "pDebugOverlayPanel: 0x%X\n", pDebugOverlayPanel);

	g_pCVar->ConsoleColorPrintf(Color::Purple(), "-------------------------------\n"
												 " dude719 CSGO SDK Example 2015\n"
												 "-------------------------------\n");

	pClientModeHook = new CVMTHookManager((PDWORD*)g_pClientMode);
	oCreateMove = (tCreateMove)pClientModeHook->dwHookMethod((DWORD)hkCreateMove, 24);
	
	#ifdef DEBUG_ENABLED
	printf("hkCreateMove: 0x%X\n", (DWORD)hkCreateMove);
	#endif
	
	pPanelHook = new CVMTHookManager((PDWORD*)g_pVGuiPanel);
	oPaintTraverse = (tPaintTraverse)pPanelHook->dwHookMethod((DWORD)hkPaintTraverse, 41);

	pStudioRenderHook = new CVMTHookManager((PDWORD*)g_pStudioRender);
	oDrawModel = (tDrawModel)pStudioRenderHook->dwHookMethod((DWORD)hkDrawModel, 29);

	pMDLCacheHook = new CVMTHookManager((PDWORD*)g_pMDLCache);
	oFindMDL = (tFindMDL)pMDLCacheHook->dwHookMethod((DWORD)hkFindMDL, 10);

	SpitShit();
	GetNetVars();

	return 0;
}

//	Main Function
DWORD WINAPI DllMain(HMODULE hDll, DWORD dwReasonForCall, LPVOID lpReserved)
{
	if (dwReasonForCall == DLL_PROCESS_ATTACH)
	{
		#ifdef DEBUG_ENABLED
		utils::CreateConsole("dbg");
		#endif

		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Init, NULL, 0, NULL);
		return 1;
	}
	else if (dwReasonForCall == DLL_PROCESS_DETACH)
	{
		//pClientHook->UnHook();
		pClientModeHook->UnHook();
		pPanelHook->UnHook();
		pStudioRenderHook->UnHook();
		pMDLCacheHook->UnHook();

		FreeConsole();

		return 1;
	}
	return 0;
}

void SpitShit()
{
#ifdef DEBUG_ENABLED

	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "\n");

	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "client.dll: 0x%X\n", offys.dwClientBase);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "engine.dll: 0x%X\n", offys.dwEngineBase);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "filesystem_stdio.dll: 0x%X\n", offys.dwFileSysBase);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "vguimatsurface.dll: 0x%X\n", offys.dwVGuiBase);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "vgui2.dll: 0x%X\n", offys.dwVGui2Base);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "materialsystem.dll: 0x%X\n", offys.dwMatSysBase);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "studiorender.dll: 0x%X\n", offys.dwStudioBase);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "inputsystem.dll: 0x%X\n", offys.dwInputSysBase);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "vstdlib.dll: 0x%X\n", offys.dwVStdBase);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "datacache.dll: 0x%X\n", offys.dwDataCacheBase);

	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "clientdll: 0x%X\n", (DWORD)clientdll);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "engine: 0x%X\n", (DWORD)engine);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "modelrender: 0x%X\n", (DWORD)modelrender);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "render: 0x%X\n", (DWORD)render);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "studiorender: 0x%X\n", (DWORD)studiorender);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "filesystem: 0x%X\n", (DWORD)filesystem);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "g_pClientMode: 0x%X\n", (DWORD)g_pClientMode);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "g_pInput: 0x%X\n", (DWORD)g_pInput);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "g_pVGuiSurface: 0x%X\n", (DWORD)g_pVGuiSurface);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "g_pVGuiPanel: 0x%X\n", (DWORD)g_pVGuiPanel);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "g_pInputSystem: 0x%X\n", (DWORD)g_pInputSystem);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "g_pDebugOverlay: 0x%X\n", (DWORD)g_pDebugOverlay);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "g_pCVar: 0x%X\n", (DWORD)g_pCVar);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "g_pMDLCache: 0x%X\n", (DWORD)g_pMDLCache);

	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "g_pGlobals: 0x%X\n", (DWORD)g_pGlobals);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "g_pGlowObjectManager: 0x%X\n", g_pGlowObjectManager);
	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "g_dwTraceLine: 0x%X\n", (DWORD)offys.dwTraceLine - offys.dwClientBase);

	g_pCVar->ConsoleColorPrintf(Color::LightBlue(), "\n");

#endif
}

void GetNetVars()
{
	g_pNetVars = new CNetVars();
	offys.dwEyePosOffset = (DWORD)g_pNetVars->GetOffset("DT_BasePlayer", "m_vecViewOffset[0]");
	offys.dwVecPunchAngles = (DWORD)g_pNetVars->GetOffset("DT_BasePlayer", "m_aimPunchAngle");
	offys.dwLocal = (DWORD)g_pNetVars->GetOffset("DT_BasePlayer", "m_Local");
	offys.dwhActiveWeapon = (DWORD)g_pNetVars->GetOffset("DT_BaseCombatCharacter", "m_hActiveWeapon");
	offys.dwbIsScoped = (DWORD)g_pNetVars->GetOffset("DT_CSPlayer", "m_bIsScoped");
	offys.dwPlayerState = (DWORD)g_pNetVars->GetOffset("DT_BasePlayer", "pl");
	offys.dwEyeAngles = (DWORD)g_pNetVars->GetOffset("DT_CSPlayer", "m_angEyeAngles");
	offys.dwNextPrimaryAttack = (DWORD)g_pNetVars->GetOffset("DT_BaseCombatWeapon", "m_flNextPrimaryAttack");
	offys.dwTickBase = (DWORD)g_pNetVars->GetOffset("DT_BasePlayer", "m_nTickBase");

	#ifdef DEBUG_ENABLED
	g_pCVar->ConsoleColorPrintf(Color::Green(), "\n");
	#endif;
}
