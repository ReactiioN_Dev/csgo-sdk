#pragma once

#include <string>
#include <io.h>

namespace utils
{
	
	static unsigned long FindPattern(unsigned long start_offset, unsigned long size, unsigned char pattern[], int n)
	{
		//printf("n = %i\n", n);
		char mask[256];
		for (int i = 0; i <= n; i++)
		{
			if (i == n)
				mask[i] = '\0';
			else if (pattern[i] == 0x00)
				mask[i] = '?';
			else
				mask[i] = 'x';
		}
	
		int pos = 0;
		int searchLen = strlen(mask) - 1;
	
		for (unsigned long retAddress = start_offset; retAddress < start_offset + size; retAddress++)
		{
			if (*(unsigned char*)retAddress == pattern[pos] || mask[pos] == '?')
			{
				if (mask[pos + 1] == '\0')
					return (retAddress - searchLen);
				pos++;
			}
			else
				pos = 0;
		}
		return 0;
	}

	static void CreateConsole(const char* name)
	{
		HANDLE lStdHandle = 0;
		int hConHandle = 0;
		FILE *fp = 0;
		AllocConsole();
		lStdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		hConHandle = _open_osfhandle(PtrToUlong(lStdHandle), 0x4000);
		SetConsoleTitleA(name);
		SetConsoleTextAttribute(lStdHandle, FOREGROUND_GREEN | FOREGROUND_INTENSITY | BACKGROUND_RED);
		fp = _fdopen(hConHandle, "w");
		*stdout = *fp;
		setvbuf(stdout, NULL, _IONBF, 0);
	}

};